INVENTORY_FILE ?= ./inventory_cephcluster
PRIVATE_VARS ?= ./cephcluster_vars.yml
DISTRIBUTION ?= ubuntu
CEPH_INVENTORY_FILE ?= ./inventory_ceph

.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE) (for node build)"
	@echo "DISTRIBUTION=$(DISTRIBUTION)"
	@echo "CEPH_INVENTORY_FILE=$(CEPH_INVENTORY_FILE) (for Ceph build)"

all: build

build_nodes:  ## Build nodes based on heat-cluster
	cd ska-cicd-heat-cluster && \
	make build INVENTORY_FILE=../$(INVENTORY_FILE) PRIVATE_VARS=../$(PRIVATE_VARS)

build_nodes_docker:  ## Build nodes docker from on heat-cluster
	cd ska-cicd-heat-cluster && \
	make build_docker INVENTORY_FILE=../$(INVENTORY_FILE) PRIVATE_VARS=../$(PRIVATE_VARS)

clean_nodes:  ## Clean nodes based on heat-cluster - CAUTION THIS DELETES EVERYTHING!!!
	cd ska-cicd-heat-cluster && \
	make clean INVENTORY_FILE=../$(INVENTORY_FILE) PRIVATE_VARS=../$(PRIVATE_VARS)

check_nodes:  ## Check nodes based on heat-cluster
	cd ska-cicd-heat-cluster && \
	make check INVENTORY_FILE=../$(INVENTORY_FILE) PRIVATE_VARS=../$(PRIVATE_VARS)

ceph_vars:
	@echo "override playbooks and variables:"
	cp -f playbooks/site.yml ceph-ansible/site.yml
	cp -f playbooks/purge-cluster.yml ceph-ansible/purge-cluster.yml
	cp -f playbooks/roles/ceph-container-engine/vars/Ubuntu-18.yml \
	      ceph-ansible/roles/ceph-container-engine/vars/Ubuntu-18.yml
	cp -f playbooks/roles/ceph-container-engine/vars/RedHat.yml \
	      ceph-ansible/roles/ceph-container-engine/vars/RedHat.yml
	cp -f playbooks/group_vars/$(DISTRIBUTION)/*.yml ceph-ansible/group_vars/

make_inv_vars:
	perl -ne 'next unless $$_ =~ /master\-/; ($$h,$$d1,$$d2) = $$_ =~ /^([^\s]+).*?data_vol_diskid\=\"([^\"]+)\"\s+data2_vol_diskid\=\"([^\"]+)\"/; print "$$h ceph_data_device1=/dev/disk/by-id/virtio-$$d1 ceph_wal_device1=/dev/disk/by-id/virtio-$$d2\n"' $(INVENTORY_FILE)

build_ceph: ceph_vars ## Build Ceph using project ceph-ansible
	# cd ceph-ansible && ansible-playbook -i ../$(CEPH_INVENTORY_FILE) site.yml --skip-tags "with_pkg"
	cd ceph-ansible && ansible-playbook -i ../$(CEPH_INVENTORY_FILE) site.yml \
			-e ../@$(PRIVATE_VARS)

add_logging:  ## Add journalbeat logging to cluster
	ansible-playbook \
			-i ./$(CEPH_INVENTORY_FILE) playbooks/add-logging.yml \
			-e @$(PRIVATE_VARS)

build: build_nodes build_nodes_docker  ## Build nodes and prepares Docker

lint: ## Lint check playbook
	ansible-lint playbooks/create-disks.yml playbooks/destroy-disks.yml

create-lvms: ## Create LVMs
	ansible-playbook \
			-i ./$(CEPH_INVENTORY_FILE) playbooks/create-disks.yml \
			-e ireallymeanit=yes

destroy-lvms: ## Destroy LVMs
	ansible-playbook \
			-i ./$(CEPH_INVENTORY_FILE) playbooks/destroy-disks.yml -e ireallymeanit=yes

purge: ceph_vars ## Purge the Ceph cluster
	cd ceph-ansible && ansible-playbook -i ../$(CEPH_INVENTORY_FILE) \
	infrastructure-playbooks/purge-cluster.yml -e ireallymeanit=yes

update: ceph_vars ## Rolling update Ceph cluster
	cd ceph-ansible && cp infrastructure-playbooks/rolling_update.yml \
	rolling_update.yml
	cd ceph-ansible && ansible-playbook -i ../$(CEPH_INVENTORY_FILE) \
	rolling_update.yml



##################################################
# additional helpers

RBD_POOL=volumes
RBD_IMAGE=foo
RBD_MOUNT=/mnt/ceph-block-device

testrbd:
	# setup and test
	# https://docs.ceph.com/docs/master/start/quick-rbd/
	# https://rhcs-test-drive.readthedocs.io/en/latest/Module-1/
	sudo rbd --cluster ceph pool init $(RBD_POOL)
	sudo rbd --cluster ceph --name client.admin --pool $(RBD_POOL) create $(RBD_IMAGE) --size 4096 --image-feature layering
	sudo rbd --cluster ceph --name client.admin --pool $(RBD_POOL) map $(RBD_IMAGE)
	sudo mkfs.ext4 -m0 /dev/rbd/$(RBD_POOL)/$(RBD_IMAGE)
	sudo mkdir $(RBD_MOUNT)
	sudo mount /dev/rbd/$(RBD_POOL)/$(RBD_IMAGE) $(RBD_MOUNT)
	ls -latr $(RBD_MOUNT)
	df $(RBD_MOUNT)
	date | sudo tee -a $(RBD_MOUNT)/test.txt
	ls -latr $(RBD_MOUNT)
	sudo umount $(RBD_MOUNT)
	ls -latr $(RBD_MOUNT)
	sudo mount /dev/rbd/$(RBD_POOL)/$(RBD_IMAGE) $(RBD_MOUNT)
	ls -latr $(RBD_MOUNT)
	sudo cat $(RBD_MOUNT)/test.txt
	# destroy
	sudo umount $(RBD_MOUNT)
	sudo rbd --cluster ceph --name client.admin --pool $(RBD_POOL) unmap $(RBD_IMAGE)
	sudo rbd --cluster ceph --name client.admin --pool $(RBD_POOL) remove $(RBD_IMAGE)
	sudo rm -rf $(RBD_MOUNT)

CEPHFS_FS=cephfs
CEPHFS_MOUNT=/mnt/ceph-fs

testcephfs:
	# setup and test
	# https://docs.ceph.com/docs/master/start/quick-cephfs/
	# https://docs.ceph.com/docs/master/cephfs/createfs/
	#
	# sudo ceph --cluster ceph osd pool create $(CEPHFS_FS)_data 256
	# sudo ceph --cluster ceph osd pool create $(CEPHFS_FS)_metadata 256
	# sudo ceph --cluster ceph fs new $(CEPHFS_FS) $(CEPHFS_FS)_metadata $(CEPHFS_FS)_data
	sudo ceph --cluster ceph fs ls
	sudo ceph --cluster ceph mds stat
	sudo mkdir -p $(CEPHFS_MOUNT)
	sudo mount -t ceph :/ $(CEPHFS_MOUNT) -o mds_namespace=$(CEPHFS_FS) -o name=admin
	sudo ls -latr $(CEPHFS_MOUNT)
	date | sudo tee -a $(CEPHFS_MOUNT)/test.txt
	sudo umount $(CEPHFS_MOUNT)
	sudo mount -t ceph :/ $(CEPHFS_MOUNT) -o mds_namespace=$(CEPHFS_FS) -o name=admin
	sudo ls -latr $(CEPHFS_MOUNT)
	sudo cat $(CEPHFS_MOUNT)/test.txt
	sudo umount $(CEPHFS_MOUNT)
	sudo rmdir $(CEPHFS_MOUNT)
	# sudo ceph --cluster ceph fs rm $(CEPHFS_FS) --yes-i-really-mean-it
	# sudo ceph --cluster ceph osd pool delete $(CEPHFS_FS)_data $(CEPHFS_FS)_data --yes-i-really-really-mean-it
	# sudo ceph --cluster ceph osd pool delete $(CEPHFS_FS)_metadata $(CEPHFS_FS)_metadata --yes-i-really-really-mean-it


RGW_USER=test
RGW_CONT=container1

testrgw:
	# setup test accounts
	# https://rhcs-test-drive.readthedocs.io/en/latest/Module-3/
	sudo radosgw-admin user create --uid="$(RGW_USER)" --display-name="$(RGW_USER) API User" --access-key="$(RGW_USER)" --secret-key="$(RGW_USER)key"
	sudo radosgw-admin subuser create --uid="$(RGW_USER)" --subuser="$(RGW_USER):swift" --secret-key="$(RGW_USER)key" --access=full
	DEVICE=`ip link | grep 'state UP' | awk '{print $$2}' | sed 's/://'` && \
	IP=`ifconfig $${DEVICE} | grep inet | head -1 | awk '{print $$2}'` && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" list && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" post $(RGW_CONT) && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" list && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" upload $(RGW_CONT) /etc/hosts && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" list && \
	swift -A http://$${IP}:8180/auth/  -U "$(RGW_USER):swift"  -K "$(RGW_USER)key" download $(RGW_CONT) etc/hosts -o - && \
	s3cmd --access_key=$(RGW_USER) --secret_key=$(RGW_USER)key --host=$${IP}:8180  --no-ssl ls && \
	s3cmd --access_key=$(RGW_USER) --secret_key=$(RGW_USER)key --host=$${IP}:8180  --no-ssl mb s3://$(RGW_CONT)s3made && \
	s3cmd --access_key=$(RGW_USER) --secret_key=$(RGW_USER)key --host=$${IP}:8180  --no-ssl info s3://$(RGW_CONT)/etc/hosts && \
	s3cmd --access_key=$(RGW_USER) --secret_key=$(RGW_USER)key --host=$${IP}:8180  --no-ssl ls s3://$(RGW_CONT)/etc/ && \
	s3cmd --access_key=$(RGW_USER) --secret_key=$(RGW_USER)key --host=$${IP}:8180  --no-ssl get s3://$(RGW_CONT)/etc/hosts -



# sorting out pg_nums
# https://stackoverflow.com/questions/39589696/ceph-too-many-pgs-per-osd-all-you-need-to-know
# benchmarking
# http://tracker.ceph.com/projects/ceph/wiki/Benchmark_Ceph_Cluster_Performance

# delete pools
# https://stackoverflow.com/questions/45012905/removing-pool-mon-allow-pool-delete-config-option-to-true-before-you-can-destro

# ceph osd pool stats
# ceph osd pool set scbench size 1


help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
